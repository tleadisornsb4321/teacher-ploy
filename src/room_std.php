<h2><b>ตารางเรียน</b></h2>
<div id="calendar"></div>
<script>
    document.addEventListener('DOMContentLoaded',function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl,{
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridDay,listWeek'
            }, 
            initialView: 'dayGridMonth',
            height: 650,
            events: './src/Event_std.php',

            eventClick: function(info) {
                info.jsEvent.preventDefault();

                info.el.style.borderColor = 'red';
            
            Swal.fire({
                title: info.event.title,
                icon: 'info',
                html:'<p>วันและเวลา: '+info.event.start+'</p><br>เข้าเรียน:<a  target="_blank" rel="noopener noreferrer" href= "'+info.event.url+'" class="warning"> '+info.event.url+'</a>',
            });
            }
        });

        calendar.render();
    });
</script> 