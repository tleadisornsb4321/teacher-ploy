<aside>
            <div class="top">
                <div class="logo">
                    <img src="assets/img/logo3.png" alt="">
                    <h2>TEACHER<span class="danger">PLOY</span></h2>
                </div>
                <div class="close" id="close-btn">
                    <i class="fa-solid fa-xmark"></i>
                </div>
            </div>

            <div class="sidebar">
                <a href="teacher.php?page=timetable">
                    <i class="fa-solid fa-calendar-days"></i>
                    <h3>ตารางเรียน</h3>
                </a>
                <a href="teacher.php?page=payment">
                    <i class="fa-solid fa-border-all"></i>
                    <h3>สถานะการจ่ายเงิน</h3>
                </a>
                <a href="teacher.php?page=room">
                    <i class="fa-solid fa-school"></i>
                    <h3>ห้องเรียน</h3>
                </a>
                <!-- <a href="" class="active"> -->
                <a href="teacher.php?page=homework">
                 <i class="fa-solid fa-book-open"></i>
                    <h3>แบบทดสอบ</h3>
                </a>
                <a href="teacher.php?page=news">
                    <i class="fa-solid fa-newspaper"></i>
                    <h3>ข่าวสาร</h3>
                </a>
                <!-- <a href="teacher.php?page=news">
                <i class="fa-solid fa-graduation-cap"></i>
                    <h3>นักเรียน</h3>
                </a> -->
                
                <a href="teacher.php?logout='1'">
                <i class="fa-solid fa-right-from-bracket"></i>
                    <h3>ออกจากระบบ</h3>
                </a>
            </div>
        </aside>