<?php
    session_start();
    if (!isset($_SESSION['extra_c_acc_user'])) {
      header('location: index.php');
    }
    if (isset($_GET['logout'])) {
      session_destroy();
      unset($_SESSION['extra_c_acc_user']);
      header('location: index.php');
    }
    require("src/conn.php");
    $usernames=$_SESSION['extra_c_acc_user'];
    mysqli_query($conn,"SET CHARACTER SET UTF8");
    $sql="SELECT * FROM extra_c_account 
    INNER JOIN extra_c_img_icon ON extra_c_account.extra_c_acc_img_id =extra_c_img_icon.extra_c_img_icon_id
    INNER JOIN extra_c_status ON extra_c_account.extra_c_acc_status =extra_c_status.extra_c_status_id
    WHERE extra_c_acc_user='$usernames'";
    $result=mysqli_query($conn,$sql);
?>
<!DOCTYPE html>
<html lang="en" oncontextmenu="return false" onselectstart="return false" ondragstart="return false">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="https://kit.fontawesome.com/0949ce2d03.js" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/logo3.png">
    <link rel="stylesheet" href="assets/css/btn.css">
    <link rel="stylesheet" href="assets/css/insert.css">
    <link rel="stylesheet" href="assets/css/form.css">
    <link rel="stylesheet" href="assets/css/card_stdNEWS.css">
    <title>TEACHER PLOY</title>
    <link rel="stylesheet" href="assets/js/fullcalendar/lib/main.css">
    <script src="assets/js/fullcalendar/lib/main.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- <script type="text/javascript">
        // document.getElementById("ctrl").setAttribute('disabled', true);
        document.onkeydown = function(){
            return false;
        }
    </script> -->

</head>
<body>
    <div class="container">
    <?php include("src/menu/menu_std.php"); ?>
        <main>
            <div class="recent-orders">
                <?php 
                if(isset($_GET["page"])){
                    $page = $_GET["page"];
                    include("src/$page.php");
                }
                else{
                    include("src/timetable_std.php");
                }
                ?>
            </div>
        </main>
        <div class="right">
            <div class="top">
                <button id="menu-btn">
                    <i class="fa-solid fa-bars"></i>
                </button>
                <div class="profile">
                    <?php while($row = mysqli_fetch_array($result)){ ?>
                    <div class="info">
                        <p>Hey, <b><?php echo $row['extra_c_acc_fname_s']?></b></p>
                     
                        <small class="text-muted"><?php echo $row['extra_c_status_name']?></small>
                    </div>
                    <div class="profile-photo">
                        <img src="<?php echo $row['extra_c_img_icon_path'] ?>" alt="">
                    </div>
                    <?php } ?>   
                </div>
            </div>
            <div class="recent-updates">
                <?php
                    if(isset($_GET['page'])){
                        $page = $_GET["page"];
                        if($page == 'timetable_std'){
                            include("src/std_menu/stady.php");
                        }
                        elseif($page == 'payment_std'){
                            include("src/std_menu/payment.php");
                        }
                    }
                    else{
                        include("src/std_menu/stady.php");
                    }
                ?>
            </div>
        </div>
    </div>
    <script src="./assets/js/index.js"></script>

</body>
</html>