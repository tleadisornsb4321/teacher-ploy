<?php 
    require("src/conn.php");
    mysqli_query($conn,"SET CHARACTER SET UTF8");
    $sql_status="SELECT * FROM extra_c_status";
    $qury_status=mysqli_query($conn, $sql_status);

    $sql_pp="SELECT * FROM extra_c_extension_people";
    $qury_pp=mysqli_query($conn, $sql_pp);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/0949ce2d03.js" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/logotest.png">
    <title>TEACHER PLOY</title>
    <link rel="stylesheet" href="assets/css/register.css">
</head>
<body>
<!-- <i class="fa-solid fa-user"></i> -->
    <!-- <a href="3fb8fb4e7f46f6f3729d0d7ab6b7a54ba41014df">student</a> -->
    <!-- <i class="fa-light fa-head-side-cough"></i><i class="fa-light fa-ear-listen"></i>
    <i class="fa-light fa-people-arrows"></i><i class="fa-light fa-book"></i>
    <label for="">เข้าสู่ระบบ</label>
    <form action="src/src/login.php" method="post">
        <input type="text" name="user" id="" value="" required> 
        <input type="password" name="password" id="" required>
        <input type="submit" value="submit">
    </form>
    <label for="">สมัครสมาชิก</label>
    <form action="src/src/register.php" method="post">
        <input type="text" name="user" id="" value="" placeholder="user" required>
        <input type="password" name="password" id="" placeholder="password" required
        <input type="text" name="tel" id="" value="" placeholder="tel" required>
        <input type="text" name="etp" id="" value="" placeholder="etp" required>
        <input type="text" name="fname_s" id="" value="" placeholder="fname_s" required>
        <input type="text" name="lname_s" id="" value="" placeholder="lname_s" required>
        <input type="text" name="fname_ep" id="" value="" placeholder="fname_ep" required>
        <input type="text" name="lname_ep" id="" value="" placeholder="lname_ep" required>
        <input type="text" name="address" id="" value="" placeholder="address" required>
        <input type="text" name="status" id="" value="" placeholder="status" required>
        <input type="text" name="school" id="" value="" placeholder="school" required>
        <input type="submit" value="submit">
    </form> -->
    <div class="container">
        <header>สมัครสมาชิก</header>

        <form action="src/src/register.php" method="post">
            <div class="form first">
                <div class="details personal">
                    <span class="title">ข้อมูลผู้สมัคร</span>

                    <div class="fields">
                        <div class="input-field">
                            <label>ชื่อผู้ใช้งาน</label>
                            <input type="text" name="user" id="" value="" placeholder="ชื่อผู้ใช้งาน" required>
                        </div>

                        <div class="input-field">
                            <label>รหัสผ่าน</label>
                            <input type="password" name="password" id="" placeholder="รหัสผ่าน" required>
                        </div>

                        <div class="input-field">
                            <label>เบอร์โทรศัพท์</label>
                            <input type="text" name="tel" id="" value="" placeholder="เบอร์โทรศัพท์" required>
                        </div>

                        <div class="input-field">
                            <label>ผู้สมัคร</label>
                            <!-- <input type="text" name="etp" id="" value="" placeholder="etp" required> -->
                            <select name="etp" required>
                                <option disabled selected>กรุณาเลือกผู้สมัคร</option>
                                <?php while($rows = mysqli_fetch_array($qury_pp)){ ?>
                                    <option value="<?php echo $rows["extra_c_extension_id"]?>"><?php echo $rows["extra_c_extension_p"]?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="input-field">
                            <label>ระดับชั้น</label>
                            <!-- <input type="text" name="status" id="" value="" placeholder="status" required> -->
                            <select name="status" required>
                                <option disabled selected>กรุณาเลือกระดับชั้น</option>
                                <?php while($row = mysqli_fetch_array($qury_status)){ ?>
                                    <option value="<?php echo $row["extra_c_status_id"]?>"><?php echo $row["extra_c_status_name"]?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="input-field">
                            <label>สถานศึกษา หรือ ที่ทำงาน</label>
                            <input type="text" name="school" id="" value="" placeholder="สถานศึกษา หรือ ที่ทำงาน" required>
                        </div>
                    </div>
                </div>

                <div class="details ID">
                    <!-- <span class="title">Identity Details</span> -->

                    <div class="fields">
                        
                        <div class="input-field">
                            <label>ชื่อ</label>
                            <input type="text" name="fname_s" id="" value="" placeholder="ชื่อ" required>
                        </div>
                        <div class="input-field">
                            <label>นามสกุล</label>
                            <input type="text" name="lname_s" id="" value="" placeholder="นามสกุล" required>
                        </div>
                        <div class="input-field">
                            <!-- <label>ที่อยู่</label> -->
                            <!-- <input type="hidden" name="" id="" value="" placeholder="ที่อยู่"> -->
                        </div>
                        <div class="input-field">
                            <label>ชื่อผู้ปกครอง</label>
                            <input type="text" name="fname_ep" id="" value="" placeholder="ชื่อผู้ปกครอง" required>
                        </div>

                        <div class="input-field">
                            <label>นามสกุลผู้ปกครอง</label>
                            <input type="text" name="lname_ep" id="" value="" placeholder="นามสกุลผู้ปกครอง" required>
                        </div>

                        <div class="input-field">
                            <label>ที่อยู่</label>
                            <input type="text" name="address" id="" value="" placeholder="ที่อยู่" required>
                        </div>

                        <!-- <div class="input-field">
                            <label>Gender</label>
                            <select required>
                                <option disabled selected>Select gender</option>
                                <option>Male</option>
                                <option>Female</option>
                                <option>Others</option>
                            </select>
                        </div> -->
                    </div>

                    <button class="submit" name="submit">
                        <span class="">ยืนยัน</span>
                        <!-- <i class="uil uil-navigator"></i> -->
                    </button>
                </div> 
            </div>

            <!-- <div class="form second">
                

                <div class="details family">
                    <span class="title">Family Details</span>

                    <div class="fields">
                        <div class="input-field">
                            <label>Father Name</label>
                            <input type="text" placeholder="Enter father name" required>
                        </div>

                        <div class="input-field">
                            <label>Mother Name</label>
                            <input type="text" placeholder="Enter mother name" required>
                        </div>

                        <div class="input-field">
                            <label>Grandfather</label>
                            <input type="text" placeholder="Enter grandfther name" required>
                        </div>

                        <div class="input-field">
                            <label>Spouse Name</label>
                            <input type="text" placeholder="Enter spouse name" required>
                        </div>

                        <div class="input-field">
                            <label>Father in Law</label>
                            <input type="text" placeholder="Father in law name" required>
                        </div>

                        <div class="input-field">
                            <label>Mother in Law</label>
                            <input type="text" placeholder="Mother in law name" required>
                        </div>
                    </div>

                    <div class="buttons">
                        <div class="backBtn">
                            <i class="uil uil-navigator"></i>
                            <span class="btnText">Back</span>
                        </div>  -->
                        
                        <!-- <button class="sumbit">
                            <span class="btnText">Submit</span>
                            <i class="uil uil-navigator"></i>
                        </button>
                    </div>
                </div> 
            </div> -->
        </form>
    </div>
</body>
</html>
<!-- <script>
    const form = document.querySelector("form"),
        nextBtn = form.querySelector(".nextBtn"),
        backBtn = form.querySelector(".backBtn"),
        allInput = form.querySelectorAll(".first input");


nextBtn.addEventListener("click", ()=> {
    allInput.forEach(input => {
        if(input.value != ""){
            form.classList.add('secActive');
        }else{
            form.classList.remove('secActive');
        }
    })
})

backBtn.addEventListener("click", () => form.classList.remove('secActive'));

</script> -->