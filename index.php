<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="Anil z" name="author">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Eduglobal - Education & Courses HTML Template">
<meta name="keywords" content="academy, course, education, elearning, learning, education html template, university template, college template, school template, online education template, tution center template">

<!-- SITE TITLE -->
<title>Teacher Ploy</title>
<!-- Favicon Icon -->
<link rel="icon" type="image/png" sizes="16x16" href="assets/img/logotest.png">
<!-- Animation CSS -->
<link rel="stylesheet" href="asset/css/animate.css" />	
<!-- Latest Bootstrap min CSS -->
<link rel="stylesheet" href="asset/bootstrap/css/bootstrap.min.css" />
<!-- Google Font -->
<!-- <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900" rel="stylesheet" /> 
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet" /> -->
<link href="https://fonts.googleapis.com/css2?family=Kodchasan:wght@500&display=swap" rel="stylesheet"> 
<!-- Icon Font CSS -->
<script src="https://kit.fontawesome.com/0949ce2d03.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="asset/css/ionicons.min.css" />
<link rel="stylesheet" href="asset/css/themify-icons.css" />
<!-- FontAwesome CSS -->
<link rel="stylesheet" href="asset/css/all.min.css" />
<!--- owl carousel CSS-->
<link rel="stylesheet" href="asset/owlcarousel/css/owl.carousel.min.css" />
<link rel="stylesheet" href="asset/owlcarousel/css/owl.theme.css" />
<link rel="stylesheet" href="asset/owlcarousel/css/owl.theme.default.min.css" />
<!-- Magnific Popup CSS -->
<link rel="stylesheet" href="asset/css/magnific-popup.css" />
<!-- Style CSS -->
<link rel="stylesheet" href="asset/css/style.css" />
<link rel="stylesheet" href="asset/css/responsive.css" />
<link rel="stylesheet" id="layoutstyle" href="asset/color/theme.css" />



</head>

<body style="font-family: 'Kodchasan', sans-serif">

<!-- LOADER -->
<div id="preloader">
    <span class="spinner"></span>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- END LOADER --> 

<!-- START HEADER -->
<header class="header_wrap dark_skin bg-white">
	<div class="top-header bg_light_navy light_skin border-0">
         <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <ul class="contact_detail list_none text-center text-md-left">
                        <li><a href="#"><i class="ti-mobile"></i>123-456-7890</a></li>
                        <li><a href="#"><i class="ti-email"></i>info@yourmail.com</a></li>
                    </ul>
                </div>
                <div class="col-md-6">
                	<div class="d-flex flex-wrap align-items-center justify-content-md-end justify-content-center mt-2 mt-md-0">
                    	<ul class="list_none social_icons social_white text-center text-md-right">
                            <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                            <li><a href="#"><i class="ion-social-googleplus"></i></a></li>
                            <li><a href="#"><i class="ion-social-youtube-outline"></i></a></li>
                            <li><a href="#"><i class="ion-social-instagram-outline"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <!-- <div class="container">
        <nav class="navbar navbar-expand-lg"> 
            <a class="navbar-brand" href="index.html">
                <img class="logo_light" src="asset/images/logo_white.png" alt="logo" />
                <img class="logo_dark" src="asset/images/logo_dark.png" alt="logo" />
                <img class="logo_default" src="asset/images/logo_dark.png" alt="logo" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="ion-android-menu"></span> </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
				<ul class="navbar-nav">
                    <li>
                        <a class="nav-link" href="contact.html">Contact</a>
                    </li>
                    <li>
                        <a class="nav-link" href="contact.html">เข้าสู่ระบบ</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div> -->
</header>
<!-- END HEADER --> 

<!-- START SECTION BANNER -->
<section class="banner_section p-0">
    <div id="carouselExampleFade" class="banner_content_wrap banner_py_large carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active background_bg position_right_center" data-img-src="assets/img/1.jpg">
                <div class="container"><!-- STRART CONTAINER -->
                    <div class="row justify-content-end">
                        <div class="col-lg-6 col-md-7">
                            <div class="banner_content animation" data-animation="fadeInDown" data-animation-delay="0.2s">
                                <h2 class="animation text-uppercase font_style1" data-animation="fadeInDown" data-animation-delay="0.4s" style="font-family: 'Kodchasan', sans-serif">English Program</h2>
                                <p class="animation" data-animation="fadeInUp" data-animation-delay="0.6s" >ยุคสมัยใหม่เรียนออนไลน์ที่ไหนก็ได้ มีคอร์สให้เลือกหลากหลายตามความต้องการและการใช้งานของผู้เรียน
                                    เลือกวันเวลาได้และไม่จำกัดอายุ</p>
                                <a class="btn btn-default animation btn-radius" href="login.php" data-animation="fadeInUp" data-animation-delay="0.8s">เข้าสู่ระบบ</a>
                                <a class="btn btn-outline-black animation btn-radius" href="login.php" data-animation="fadeInUp" data-animation-delay="0.8s">สนใจสมัครคอร์สเรียน <i class="fa-solid fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div><!-- END CONTAINER-->
            </div>
            <div class="carousel-item background_bg" data-img-src="assets/img/2.jpg">
                <div class="container"><!-- STRART CONTAINER -->
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="banner_content animation" data-animation="fadeInDown" data-animation-delay="0.2s">
                                <h2 class="animation text-uppercase font_style1" data-animation="fadeInDown" data-animation-delay="0.4s" style="font-family: 'Kodchasan', sans-serif">เรียนภาษาอังกฤษออนไลน์ ง่ายๆได้ที่บ้าน</h2>
                                <p class="animation" data-animation="fadeInUp" data-animation-delay="0.6s">ยุคสมัยใหม่เรียนออนไลน์ที่ไหนก็ได้ มีคอร์สให้เลือกหลากหลายตามความต้องการและการใช้งานของผู้เรียน
                                    เลือกวันเวลาได้และไม่จำกัดอายุ</p>
                                <a class="btn btn-default animation btn-radius" href="login.php" data-animation="fadeInUp" data-animation-delay="0.8s">เข้าสู่ระบบ</a>
                                <a class="btn btn-outline-black animation btn-radius" href="login.php" data-animation="fadeInUp" data-animation-delay="0.8s">สนใจสมัครคอร์สเรียน <i class="fa-solid fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div><!-- END CONTAINER-->
                <div class="ol_shape13">
                    <div class="animation" data-animation="fadeInUp" data-animation-delay="0.3s">
                        <img data-parallax='{"y": -30, "smoothness": 10, "distance": 90}' src="asset/images/shape13.png" alt="shape13"/>
                    </div>
                </div>
                <div class="ol_shape14">
                    <div class="animation" data-animation="bounceInUp" data-animation-delay="0.4s">
                        <img data-parallax='{"y": -30, "smoothness": 10}' src="asset/images/shape14.png" alt="shape14"/>
                    </div>
                </div>
                <div class="ol_shape15">
                    <div class="animation" data-animation="slideInUp" data-animation-delay="0.5s">
                        <img data-parallax='{"y": 30, "smoothness": 10}' src="asset/images/shape15.png" alt="shape15"/>
                    </div>
                </div>
                <div class="ol_shape16">
                    <div class="animation" data-animation="slideInUp" data-animation-delay="0.6s">
                        <img data-parallax='{"y": -30, "smoothness": 5}' src="asset/images/shape16.png" alt="shape16"/>
                    </div>
                </div>
                <div class="ol_shape17">
                    <div class="animation" data-animation="slideInRight" data-animation-delay="0.7s">
                        <img data-parallax='{"x": 30, "smoothness": 10, "distance": 90}' src="asset/images/shape17.png" alt="shape17"/>
                    </div>
                </div>
                <div class="ol_shape18">
                    <div class="animation" data-animation="bounceInUp" data-animation-delay="0.8s">
                        <img data-parallax='{"y": -40, "smoothness": 10}' src="asset/images/shape18.png" alt="shape18"/>
                    </div>
                </div>
                <div class="ol_shape19">
                    <div class="animation" data-animation="rollIn" data-animation-delay="0.9s">
                        <img data-parallax='{"y": -30, "smoothness": 10}' src="asset/images/shape19.png" alt="shape19"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-nav carousel_style2">
            <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                <i class="ion-chevron-left"></i>
            </a>
            <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                <i class="ion-chevron-right"></i>
            </a>
        </div>
    </div>
</section>
<!-- END SECTION BANNER -->
<!-- START SECTION FEATURE -->
<section class="staggered-animation-wrap">
    <div class="container">
    	<div class="row justify-content-center">
        	<div class="col-xl-6 col-lg-8">
            	<div class="text-center animation" data-animation="fadeInUp" data-animation-delay="0.02s">
                    <div class="heading_s1 text-center">
                        <h2 class="font_style1" style="font-family: 'Kodchasan', sans-serif"><b></b>English Program</b></h2>
                    </div>
                    <p>ยุคสมัยใหม่เรียนออนไลน์ที่ไหนก็ได้ มีคอร์สให้เลือกหลากหลายตามความต้องการและการใช้งานของผู้เรียน
                        เลือกวันเวลาได้และไม่จำกัดอายุ</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="icon_box text-center bg_danger text_white icon_box_style3 box_shadow2 radius_all_10 animation" data-animation="fadeInUp" data-animation-delay="0.02s">
                	<div class="box_icon bg-white mb-3">
                		<i class="fa-solid fa-head-side-cough" style="color: #F6475F;"></i>
                    </div>
                    <div class="intro_desc">
                        <h5 style="font-family: 'Kodchasan', sans-serif">Phonics</h5>
                        <p>เรียนการออกเสียงภาษาอังกฤษ โดยใช้การผสมเสียงตัวอักษร ทำให้นักเรียนพูดภาษาอังกฤษได้อย่างถูกต้องและอ่านภาษาอังกฤษออกได้โดยไม่ต้องท่องจำ</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
            	<div class="icon_box text-center bg_blue2 text_white icon_box_style3 box_shadow2 radius_all_10 animation" data-animation="fadeInUp" data-animation-delay="0.03s">
                	<div class="box_icon bg-white mb-3">
                        <i class="fa-solid fa-ear-listen" style="color: #4382FF;"></i>
                    </div>
                    <div class="intro_desc">
                        <h5 style="font-family: 'Kodchasan', sans-serif">Listening</h5>
                        <p>ฝึกการฟังภาษาอังกฤษตามระดับภาษาอังกฤษของนักเรียน ให้นักเรียนคุ้นเคยสำเนียงและระดับความเร็วของเจ้าของภาษา รวมถึงเทคนิคต่างๆที่ช่วยพัฒนาทักษะการฟังได้เร็วยิ่งขึ้น</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
            	<div class="icon_box text-center bg_orange text_white icon_box_style3 box_shadow2 radius_all_10 animation" data-animation="fadeInUp" data-animation-delay="0.04s">
                	<div class="box_icon bg-white mb-3">
                		<i class="fa-solid fa-people-arrows" style="color: #F89035;"></i>
                    </div>
                    <div class="intro_desc">
                        <h5 style="font-family: 'Kodchasan', sans-serif">Speaking</h5>
                        <p>ฝึกการสนทนาภาษาอังกฤษที่ใช้ในชีวิตประจำวันและที่ทำงาน ให้นักเรียนมั่นใจในการพูดภาษาอังกฤษมากขึ้น มีแนวทางการเรียนการสอนที่หลากหลายโดยขึ้นอยู่กับพื้นฐานของนักเรียนเป็นหลัก</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
            	<div class="icon_box text-center bg_light_green text_white icon_box_style3 box_shadow2 radius_all_10 animation" data-animation="fadeInUp" data-animation-delay="0.02s">
                	<div class="box_icon bg-white mb-3">
                        <i class="fa-solid fa-book" style="color: #B3D369;"></i>
                    </div>
                    <div class="intro_desc">
                        <h5 style="font-family: 'Kodchasan', sans-serif">Reading and Vocabulary</h5>
                        <p>ฝึกการอ่านจับใจความให้นักเรียนเข้าใจโครงสร้างพื้นฐานของภาษาอังกฤษ แก้ปัญหานักเรียนที่รู้คำศัพท์ทุกคำแต่แปลไม่ได้ พร้อมทั้งเพิ่มคลังคำศัพท์จากสื่อการสอนที่หลากหลายตามความสนใจของนักเรียน</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
            	<div class="icon_box text-center bg_light_navy text_white icon_box_style3 box_shadow2 radius_all_10 animation" data-animation="fadeInUp" data-animation-delay="0.03s">
                	<div class="box_icon bg-white mb-3">
                        <i class="fa-solid fa-spell-check" style="color: #554DA7;"></i>
                    </div>
                    <div class="intro_desc">
                        <h5 style="font-family: 'Kodchasan', sans-serif">Grammar and Writing</h5>
                        <p>เรียนรู้ไวยากรณ์ภาษาอังกฤษเพื่อเป็นพื้นฐานในการฝึกแต่งประโยคและการเขียนเรียงความ เหมาะสำหรับนักเรียนที่ต้องการเน้นปูพื้นฐานการสอบแข่งขันและตะลุยโจทย์ภาษาอังกฤษ โดยเนื้อหาการสอนแบ่งตามระดับตั้งแต่ประถมจนถึงวัยทำงาน</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
            	<div class="icon_box text-center bg_pink text_white icon_box_style3 box_shadow2 radius_all_10 animation" data-animation="fadeInUp" data-animation-delay="0.04s">
                	<div class="box_icon bg-white mb-3">
                        <i class="fa-solid fa-pen" style="color: #F94FA4;"></i>
                    </div>
                    <div class="intro_desc">
                        <h5 style="font-family: 'Kodchasan', sans-serif">English Program (EP)</h5>
                        <p>สอนเนื้อหา รวมถึงฝึกทำแบบฝึกหัดและข้อสอบวิชาหลักอื่นๆ เพิ่มเติมจากวิชาภาษาอังกฤษในหลักสูตร English Program เช่น Science, Math, Social Studies เป็นต้น</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ol_shape20">
        <div class="staggered-animation" data-animation="bounceInDown" data-animation-delay="0.1s">
            <img data-parallax='{"y": -30, "smoothness": 20}' src="asset/images/shape20.png" alt="shape20"/>
        </div>
    </div>
    <div class="ol_shape21">
        <div class="staggered-animation" data-animation="bounceInUp" data-animation-delay="0.2s">
            <img data-parallax='{"y": -30, "smoothness": 20}' src="asset/images/shape21.png" alt="shape21"/>
        </div>
    </div>
    <div class="ol_shape22">
        <div class="staggered-animation" data-animation="bounceInRight" data-animation-delay="0.3s">
            <img data-parallax='{"y": 30, "smoothness": 20}' src="asset/images/shape22.png" alt="shape22"/>
        </div>
    </div>
    <div class="ol_shape23">
        <div class="staggered-animation" data-animation="bounceInUp" data-animation-delay="0.4s">
            <img data-parallax='{"y": -30, "smoothness": 20}' src="asset/images/shape23.png" alt="shape23"/>
        </div>
    </div>
    <div class="ol_shape24">
        <div class="staggered-animation" data-animation="bounceInLeft" data-animation-delay="0.5s">
            <img data-parallax='{"y": -30, "smoothness": 20}' src="asset/images/shape24.png" alt="shape24"/>
        </div>
    </div>
    <div class="ol_shape25">
        <div class="staggered-animation" data-animation="bounceInDown" data-animation-delay="0.6s">
            <img data-parallax='{"y": 30, "smoothness": 20}' src="asset/images/shape25.png" alt="shape25"/>
        </div>
    </div>
    <div class="ol_shape26">
        <div class="staggered-animation" data-animation="bounceInLeft" data-animation-delay="0.7s">
            <img data-parallax='{"y": -30, "smoothness": 20}' src="asset/images/shape26.png" alt="shape26"/>
        </div>
    </div>
    <div class="ol_shape27">
        <div class="staggered-animation" data-animation="bounceInLeft" data-animation-delay="0.7s">
            <img data-parallax='{"y": -30, "smoothness": 20}' src="asset/images/shape27.png" alt="shape27"/>
        </div>
    </div>
    <div class="ol_shape28">
        <div class="staggered-animation" data-animation="bounceInLeft" data-animation-delay="0.7s">
            <img data-parallax='{"y": -30, "smoothness": 20}' src="asset/images/shape28.png" alt="shape28"/>
        </div>
    </div>
</section> 
<!-- END SECTION FEATURE -->


<a href="#" class="scrollup" style="display: none;"><i class="ion-ios-arrow-up"></i></a> 

<!-- Latest jQuery --> 
<script src="asset/js/jquery-1.12.4.min.js"></script> 
<!-- jquery-ui --> 
<script src="asset/js/jquery-ui.js"></script>
<!-- popper min js --> 
<script src="asset/js/popper.min.js"></script>
<!-- Latest compiled and minified Bootstrap --> 
<script src="asset/bootstrap/js/bootstrap.min.js"></script> 
<!-- owl-carousel min js  --> 
<script src="asset/owlcarousel/js/owl.carousel.min.js"></script> 
<!-- magnific-popup min js  --> 
<script src="asset/js/magnific-popup.min.js"></script> 
<!-- waypoints min js  --> 
<script src="asset/js/waypoints.min.js"></script> 
<!-- parallax js  --> 
<script src="asset/js/parallax.js"></script> 
<!-- countdown js  --> 
<script src="asset/js/jquery.countdown.min.js"></script> 
<!-- jquery.counterup.min js --> 
<script src="asset/js/jquery.counterup.min.js"></script>
<!-- imagesloaded js --> 
<script src="asset/js/imagesloaded.pkgd.min.js"></script>
<!-- isotope min js --> 
<script src="asset/js/isotope.min.js"></script>
<!-- jquery.parallax-scroll js -->
<script src="asset/js/jquery.parallax-scroll.js"></script>
<!-- scripts js --> 
<script src="asset/js/scripts.js"></script>

</body>
</html>